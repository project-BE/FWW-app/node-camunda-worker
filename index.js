
import { Client, Variables, logger } from 'camunda-external-task-client-js';
import { Kafka } from 'kafkajs';
import { TransactioEnum } from './enum/transaction-enum.js';
import Passenger from './dto/passenger.js';
import Reservation from './dto/reservation.js';
import BookingDto from './dto/bookingDto.js';
import axios from 'axios';
import * as dotenv from 'dotenv'
import { paymentMethod } from './enum/payment-method.js';

dotenv.config()


const config = { baseUrl: 'http://localhost:8080/engine-rest', use: logger, asyncResponseTimeout: 10000 };

try {
    const kafka = new Kafka({
        clientId: 'node-camunda-worker',
        brokers: ['localhost:19092'],
        idempotent: true,
    })
    const producer = kafka.producer()
    await producer.connect()

    const client = new Client(config);

    // task worker 'invalid-flight-date'
    await client.subscribe(
        'invalid-flight-date',
        async function ({ task, taskService }) {

            const processVariables = task.variables.get('processVariables');
            const jsonProcessVariables = JSON.parse(processVariables);
            try {
                jsonProcessVariables.status = TransactioEnum.StatusBookingTransaction.FAIL
                jsonProcessVariables.message = TransactioEnum.MessageBookingTransaction.DATE_FAIL
                jsonProcessVariables['detail_message'] = `workflow invalid-flight-date success`

                await taskService.complete(task);
                console.log('completing success task with topic: invalid-flight-date')

                await producer.send({
                    topic: 'booking-transaction.reply',
                    messages: [
                        {
                            key: jsonProcessVariables.id,
                            value: JSON.stringify(jsonProcessVariables),
                            partition: 0
                        },
                    ],
                })
            }
            catch (error) {
                console.log([error], 'Error in task : invalid-flight-date ')

                // send respon error to kafka reply topic
                jsonProcessVariables['message'] = TransactioEnum.MessageBookingTransaction.FAILED_CREATE_BOOKING
                jsonProcessVariables['detail_message'] = error

                await taskService.complete(task);
                console.log('completing error task with topic: invalid-flight-date')

                await producer.send({
                    topic: 'booking-transaction.reply',
                    messages: [
                        {
                            key: jsonProcessVariables.id,
                            value: JSON.stringify(jsonProcessVariables),
                            partition: 0
                        },
                    ],
                })
            }
        });

    // worker 'invalid-book-seat'
    await client.subscribe(
        'invalid-book-seat',
        async function ({ task, taskService }) {

            const processVariables = task.variables.get('processVariables');
            const jsonProcessVariables = JSON.parse(processVariables);
            try {
                jsonProcessVariables.status = TransactioEnum.StatusBookingTransaction.FAIL
                jsonProcessVariables.message = TransactioEnum.MessageBookingTransaction.EXCEED_BOOK_QUOTA
                jsonProcessVariables['detail_message'] = `workflow invalid-book-seat success`

                await taskService.complete(task);

                console.log('completing success task with topic: invalid-book-seat')

                await producer.send({
                    topic: 'booking-transaction.reply',
                    messages: [
                        {
                            key: jsonProcessVariables.id,
                            value: JSON.stringify(jsonProcessVariables),
                            partition: 0
                        },
                    ],
                })
            }
            catch (error) {
                console.log([error], 'error in task topic: invalid-book-seat')

                // send respon error to kafka reply topic
                jsonProcessVariables['message'] = TransactioEnum.MessageBookingTransaction.FAILED_CREATE_BOOKING
                jsonProcessVariables['detail_message'] = error

                await taskService.complete(task);

                console.log('completing error task with topic: invalid-book-seat')

                await producer.send({
                    topic: 'booking-transaction.reply',
                    messages: [
                        {
                            key: jsonProcessVariables.id,
                            value: JSON.stringify(jsonProcessVariables),
                            partition: 0
                        },
                    ],
                })
            }
        })

    // worker 'invalid-regulation'
    await client.subscribe(
        'invalid-regulation',
        async function ({ task, taskService }) {

            const processVariables = task.variables.get('processVariables');
            const jsonProcessVariables = JSON.parse(processVariables);
            try {
                jsonProcessVariables.status = TransactioEnum.StatusBookingTransaction.FAIL
                jsonProcessVariables.message = TransactioEnum.MessageBookingTransaction.REGULATION_FAIL
                jsonProcessVariables['detail_message'] = `workflow invalid-regulation success`

                await taskService.complete(task);

                console.log('completing success task with topic: invalid-regulation')

                await producer.send({
                    topic: 'booking-transaction.reply',
                    messages: [
                        {
                            key: jsonProcessVariables.id,
                            value: JSON.stringify(jsonProcessVariables),
                            partition: 0
                        },
                    ],
                })
            } catch (error) {
                console.log([error], 'error in task topic: invalid-regulation')

                // send respon error to kafka reply topic
                jsonProcessVariables['message'] = TransactioEnum.MessageBookingTransaction.FAILED_CREATE_BOOKING
                jsonProcessVariables['detail_message'] = error

                await taskService.complete(task);

                console.log('completing error task with topic: invalid-regulation')

                await producer.send({
                    topic: 'booking-transaction.reply',
                    messages: [
                        {
                            key: jsonProcessVariables.id,
                            value: JSON.stringify(jsonProcessVariables),
                            partition: 0
                        },
                    ],
                })
            }
        })

    // worker 'book-seat-task'
    await client.subscribe(
        'book-seat-task',
        async function ({ task, taskService }) {

            const processVariables = task.variables.get('processVariables');
            const jsonProcessVariables = JSON.parse(processVariables);

            try {
                // Create instances of Passenger and Reservation
                const passengerInstance = new Passenger(
                    jsonProcessVariables.passenger.first_name,
                    jsonProcessVariables.passenger.last_name,
                    jsonProcessVariables.passenger.email,
                    jsonProcessVariables.passenger.phone_number,
                    jsonProcessVariables.passenger.nik,
                );
                const reservationInstance = new Reservation(
                    jsonProcessVariables.reservation.flight_id,
                    jsonProcessVariables.reservation.transaction_id,
                    jsonProcessVariables.reservation.booking_code,
                    jsonProcessVariables.reservation.seat_id,
                    jsonProcessVariables.reservation.user_id,
                );

                // Create an instance of the BookingDto class using the instances of Passenger and Reservation
                const bookingDtoInstance = new BookingDto(reservationInstance, passengerInstance);
                const newProcessVariables = new Variables();
                newProcessVariables.set('bookingDto', bookingDtoInstance);

                const axiosConfig = {
                    baseUrl: process.env.SERVICE_CORE_HOST,
                    method: 'post',
                    url: '/book/seat',
                    data: bookingDtoInstance
                }
                const coreUrl = `${process.env.SERVICE_CORE_HOST}/book/seat`
                const result = await axios.post(coreUrl, bookingDtoInstance)
                jsonProcessVariables['booking_status_code'] = result.statusCode
                jsonProcessVariables.message = TransactioEnum.MessageBookingTransaction.BOOK_SUCCESS
                jsonProcessVariables['detail_message'] = `workflow book-seat-task success`
                jsonProcessVariables['payment_method'] = paymentMethod

                // log success book a seat
                console.log('completing success task with topic: book-seat-task')
                await taskService.complete(task, bookingDtoInstance);

                await producer.send({
                    topic: 'booking-transaction.reply',
                    messages: [
                        {
                            key: jsonProcessVariables.id,
                            value: JSON.stringify(jsonProcessVariables),
                            partition: 0
                        },
                    ],
                })
            } catch (error) {
                console.log([error], 'error in executing task topic: book-seat-task')
                delete jsonProcessVariables.reservation.booking_code;
                jsonProcessVariables['booking_status_code'] = error?.statusCode;
                jsonProcessVariables.message = TransactioEnum.MessageBookingTransaction.FAILED_CREATE_BOOKING
                jsonProcessVariables['detail_message'] = error

                console.log('completing error task with topic: book-seat-task')
                await taskService.complete(task);
                await producer.send({
                    topic: 'booking-transaction.reply',
                    messages: [
                        {
                            key: jsonProcessVariables.id,
                            value: JSON.stringify(jsonProcessVariables),
                            partition: 0
                        },
                    ],
                })
            }
        })

    // worker 'check-book-code-eligibility'
    await client.subscribe(
        'check-book-code-eligibility',
        async function ({ task, taskService }) {

            const processPaymentVariables = task.variables.get('processPaymentVariables');
            const hoursValidAfterBought = task.variables.get('hoursValidAfterBought')
            const jsonProcessVariables = JSON.parse(processPaymentVariables);

            try {
                const bookingCodeDBDate = jsonProcessVariables['payment_message']['bookingcode_date']
                // check booking_code is valid (valid until 1 day before flight_date )
                // rule checking for valid before 6 hours
                const dateNow = new Date()
                const validUntil = new Date(bookingCodeDBDate)
                validUntil.setHours(validUntil.getHours() + hoursValidAfterBought)

                if (dateNow < validUntil) {
                    const variables = new Variables()
                    const isEligibleForPayment = variables.set('isEligibleForPayment', true)
                    jsonProcessVariables.message = TransactioEnum.MessageBookingTransaction.SUCCESS_PAYMENT_TRANSACTION
                    jsonProcessVariables['detail_message'] = `workflow check-book-code-eligibility`

                    // log success task with topic check-book-code-eligibility
                    console.log('completing success task with topic: check-book-code-eligibility')

                    await taskService.complete(task, isEligibleForPayment);

                } else {
                    // invalid book code
                    // compensate db to rollback
                    const variables = new Variables()
                    const isEligibleForPayment = variables.set('isEligibleForPayment', false)
                    const compensateUrl = `${process.env.SERVICE_CORE_HOST}/book/rollback?bookingCode=${jsonProcessVariables.payment_message.booking_code}`
                    const result = await axios.get(compensateUrl)
                    jsonProcessVariables['booking_rollback_code'] = result?.data?.statusCode
                    jsonProcessVariables.message = TransactioEnum.MessageBookingTransaction.FAILED_EXPIRED_BOOKING_CODE
                    jsonProcessVariables['detail_message'] = `workflow rollback code book via topic check-book-code-eligibility`

                    console.log('completing rollback in task check-book-code-eligibility')
                    await taskService.complete(task, isEligibleForPayment);

                    await producer.send({
                        topic: 'payment-transaction.reply',
                        messages: [
                            {
                                key: jsonProcessVariables.id,
                                value: JSON.stringify(jsonProcessVariables),
                                partition: 0
                            },
                        ],
                    })
                }
            } catch (error) {
                console.log([error], ' error completing task with topic: check-book-code-eligibility')
                jsonProcessVariables.message = TransactioEnum.MessageBookingTransaction.FAILED_PAYMENT_TRANSACTION
                jsonProcessVariables['detail_message'] = `error executing workflow topic check-book-code-eligibility`
                const isEligibleForPayment = variables.set('isEligibleForPayment', false)

                await taskService.complete(task, isEligibleForPayment);
                await producer.send({
                    topic: 'booking-transaction.reply',
                    messages: [
                        {
                            key: jsonProcessVariables.id,
                            value: JSON.stringify(jsonProcessVariables),
                            partition: 0
                        },
                    ],
                })
            }
        })

    await client.subscribe('make-payment',
        async function ({ task, taskService }) {
            const processPaymentVariables = task.variables.get('processPaymentVariables');
            const hoursBookCodeValidBeforeFlight = task.variables.get('hoursBookCodeValidBeforeFlight');
            const isEligibleForPayment = task.variables.get('isEligibleForPayment')
            const payload = JSON.parse(processPaymentVariables);

            try {
                if (isEligibleForPayment) {
                    // make payment
                    const paymentUrl = process.env.PAYMENT_URL
                    const paymentRespon = await axios.post(paymentUrl, payload)
                    if (paymentRespon.data.code === '000') {

                        // get reservation code
                        const resCodeUrl = `${process.env.SERVICE_CORE_HOST}/book/reservationcode?bookingCode=${payload.payment_message.booking_code}`
                        const reservationCode = await axios.get(resCodeUrl)

                        payload.message = TransactioEnum.MessageBookingTransaction.SUCCESS_PAYMENT_TRANSACTION
                        payload['detail-message'] = `success execute payment of booking-code: ${payload.payment_message.booking_code}`
                        payload['reservation_code'] = reservationCode.data.result

                        // make payment
                        const updatePaymentUrl = `${process.env.SERVICE_CORE_HOST}/book/payment`
                        const payloadUpdatePayment = {
                            booking_code: payload.payment_message.booking_code,
                            reservation_code: reservationCode.data.result,
                            transaction_madtrins_id: payload.payment_message.transaction_madtrins_id,
                            payment_type: payload.payment_message.payment_method_name,
                            payment_date: new Date(),
                        }
                        const resUpdatePayment = await axios.post(updatePaymentUrl, payloadUpdatePayment)
                        payload['update_payment_info'] = resUpdatePayment.data.result

                        await producer.send({
                            topic: 'payment-transaction.reply',
                            messages: [
                                {
                                    key: payload.id,
                                    value: JSON.stringify(payload),
                                    partition: 0
                                },
                            ],
                        })
                        console.log(`completing success task with topic: make-payment`)
                        // complete task
                        const variables = new Variables()
                        // const paymentData = variables.set('paymentData', resUpdatePayment.data.result)
                        const isPaymentSucces = variables.set('isPaymentSucces', true)
                        await taskService.complete(task, isPaymentSucces);

                    } else {
                        jsonProcessVariables.message = TransactioEnum.MessageBookingTransaction.FAILED_PAYMENT_TRANSACTION
                        payload['detail-message'] = `failed call api to madtrins, payment of booking-code: ${payload.payment_message.booking_code}`
                        await producer.send({
                            topic: 'payment-transaction.reply',
                            messages: [
                                {
                                    key: jsonProcessVariables.id,
                                    value: JSON.stringify(jsonProcessVariables),
                                    partition: 0
                                },
                            ],
                        })

                        // log failed payment call api
                        console.log(`completing task failed call-api to madtrins, task with topic: make-payment`)
                        const variables = new Variables()
                        const isPaymentSucces = variables.set('isPaymentSucces', false)
                        await taskService.complete(task, isPaymentSucces);
                    }
                } else {
                    console.log(`un-eligible payment of booking code ${payload.payment_message.booking_code}`)
                    payload.message = TransactioEnum.MessageBookingTransaction.FAILED_PAYMENT_TRANSACTION
                    payload['detail-message'] = `un-eligible payment of booking-code: ${payload.payment_message.booking_code}`
                    await producer.send({
                        topic: 'payment-transaction.reply',
                        messages: [
                            {
                                key: payload.id,
                                value: JSON.stringify(payload),
                                partition: 0
                            },
                        ],
                    })
                    const variables = new Variables()
                    const isPaymentSucces = variables.set('isPaymentSucces', false)
                    await taskService.complete(task, isPaymentSucces);
                }
            } catch (error) {
                console.log([error], `error perform payment of booking-code: ${payload.payment_message.booking_code}`)
                payload.message = TransactioEnum.MessageBookingTransaction.FAILED_PAYMENT_TRANSACTION
                payload['detail-message'] = `error perform payment of booking-code: ${payload.payment_message.booking_code}`
                await producer.send({
                    topic: 'payment-transaction.reply',
                    messages: [
                        {
                            key: payload.id,
                            value: JSON.stringify(payload),
                            partition: 0
                        },
                    ],
                })
                const variables = new Variables()
                const isPaymentSucces = variables.set('isPaymentSucces', false)
                await taskService.complete(task, isPaymentSucces);
            }
        })

    await client.subscribe('send-email', async function ({ task, taskService }) {

        const processPaymentVariables = task.variables.get('processPaymentVariables');
        const paymentData = task.variables.get('paymentData')
        const payload = JSON.parse(processPaymentVariables)

        try {
            if (paymentData) {
                // get email info
                const emailInfoUrl = `${process.env.SERVICE_CORE_HOST}/book/passengeremail?bookingCode=${payload.payment_message.booking_code}`
                const resEmailInfo = await axios.get(emailInfoUrl)

                // send email
                const sendmailUrl = process.env.SENDMAIL_URL;
                const sendmailPayload = {
                    email_recipient: resEmailInfo.data.result.email,
                    first_name: resEmailInfo.data.result.first_name,
                    last_name: resEmailInfo.data.result.last_name
                }
                const responMail = await axios.post(sendmailUrl, sendmailPayload)
                if (responMail.data.status === 'success') {
                    console.log(`completing success task with topic: send-email`)
                    await taskService.complete(task)
                } else {
                    console.log(`completing failed task with topic: send-email`)
                    await taskService.complete(task)
                }
            }
        } catch (error) {
            console.log([error], 'error sending mail')
            await taskService.complete(task)
        }
    })

    await client.subscribe('check-reservation-code-eligibility', async function ({ task, taskService }) {

        const processRedeemTicketVariables = task.variables.get('processRedeemTicketVariables');
        const hoursBookCodeValidBeforeFlight = task.variables.get('hoursBookCodeValidBeforeFlight');
        const data = JSON.parse(processRedeemTicketVariables)

        try {
            // check booking_code is valid (valid until 1 day before flight_date )
            // rule checking for valid before 6 hours
            const dateNow = new Date()
            const validUntil = new Date(data['departure_time'])
            validUntil.setHours(validUntil.getHours() - hoursBookCodeValidBeforeFlight)

            const variables = new Variables()
            if (dateNow < validUntil) {
                // eligible to board
                const isValidBoarding = variables.set('isValidBoarding', true)
                await taskService.complete(task, isValidBoarding)
                console.log(`[ ${data.transaction_id} ] completing success task with topic: check-reservation-code-eligibility, eligible to proceed boarding `)
            } else {
                // NOT eligible to board
                const isValidBoarding = variables.set('isValidBoarding', false)
                await taskService.complete(task, isValidBoarding)
                console.log(`[ ${data.transaction_id} ] completing un-eligible to board task with topic: check-reservation-code-eligibility `)

                data.message = TransactioEnum.MessageBookingTransaction.FAILED_REDEEM_TRANSACTION
                data['detail_message'] = `un-eligible to board, expired reservation_code`
                await producer.send({
                    topic: 'boarding-transaction.reply',
                    messages: [
                        {
                            key: data.id,
                            value: JSON.stringify(data),
                            partition: 0
                        },
                    ],
                })
            }
        } catch (error) {
            console.log([error], `[ ${data.transaction_id} ] error process reservation code`);
            const isValidBoarding = variables.set('isValidBoarding', false)
            await taskService.complete(task, isValidBoarding)
            console.log(`[ ${data.transaction_id} ] completing error process to board task with topic: check-reservation-code-eligibility `)

            data.message = TransactioEnum.MessageBookingTransaction.FAILED_REDEEM_TRANSACTION
            data['detail_message'] = `error processing boarding workflow`
            await producer.send({
                topic: 'boarding-transaction.reply',
                messages: [
                    {
                        key: data.id,
                        value: JSON.stringify(data),
                        partition: 0
                    },
                ],
            })
        }
    })

    await client.subscribe('get-ticket', async function ({ task, taskService }) {

        const processRedeemTicketVariables = task.variables.get('processRedeemTicketVariables');
        const isValidBoarding = task.variables.get('isValidBoarding')
        const data = JSON.parse(processRedeemTicketVariables)

        try {
            if (isValidBoarding) {
                console.log(`[ ${data.transaction_id} ] completing success task with topic: get-ticket`)
                // get ticket
                await taskService.complete(task)

            } else {
                // send invalid to get ticket
                console.log(`[ ${data.transaction_id} ] completing failed task with topic: get-ticket`)

                data.message = TransactioEnum.MessageBookingTransaction.FAILED_REDEEM_TRANSACTION
                data['detail_message'] = `un-eligible to board, expired reservation_code, task topic: get-ticket`
                await taskService.complete(task)
                await producer.send({
                    topic: 'boarding-transaction.reply',
                    messages: [
                        {
                            key: data.id,
                            value: JSON.stringify(data),
                            partition: 0
                        },
                    ],
                })

            }
        } catch (error) {
            console.log([error], `[ ${data.transaction_id} ] error processing workflow of topic: get-ticket`)
            data.message = TransactioEnum.MessageBookingTransaction.FAILED_REDEEM_TRANSACTION
            data['detail_message'] = `error processing workflow task topic: get-ticket`
            await taskService.complete(task)
            await producer.send({
                topic: 'boarding-transaction.reply',
                messages: [
                    {
                        key: data.id,
                        value: JSON.stringify(data),
                        partition: 0
                    },
                ],
            })

        }
    })

    await client.subscribe('onboard-plane', async function ({ task, taskService }) {

        const processRedeemTicketVariables = task.variables.get('processRedeemTicketVariables');
        const isValidBoarding = task.variables.get('isValidBoarding')
        const data = JSON.parse(processRedeemTicketVariables)
        try {
            if (isValidBoarding) {
                // update boarding
                const onBoarUrl = `${process.env.SERVICE_CORE_HOST}/book/onboard?reservationCode=${data['reservation_code']}`

                const resultUpdateDb = await axios.get(onBoarUrl);
                if (resultUpdateDb?.data?.statusCode === 200) {
                    data.message = TransactioEnum.MessageBookingTransaction.SUCCESS_REDEEM_TRANSACTION
                    data['detail_message'] = `success process on board the plane for reservation code ; ${data['reservation_code']}`
                    console.log(`[ ${data.transaction_id} ] completing success task with topic: onboard-plane`)
                    await taskService.complete(task)

                    await producer.send({
                        topic: 'boarding-transaction.reply',
                        messages: [
                            {
                                key: data.id,
                                value: JSON.stringify(data),
                                partition: 0
                            },
                        ],
                    })
                } else {

                    data.message = TransactioEnum.MessageBookingTransaction.FAILED_REDEEM_TRANSACTION
                    data['detail_message'] = `failed process update onboard db data for reservation code ; ${data['reservation_code']}`
                    console.log(`[ ${data.transaction_id} ] failed process update onboard db data, task with topic: onboard-plane`)
                    await taskService.complete(task)

                    await producer.send({
                        topic: 'boarding-transaction.reply',
                        messages: [
                            {
                                key: data.id,
                                value: JSON.stringify(data),
                                partition: 0
                            },
                        ],
                    })

                }
                
            } else {
                data.message = TransactioEnum.MessageBookingTransaction.FAILED_REDEEM_TRANSACTION
                data['detail_message'] = `un eligible to onboard plane for reservation code ; ${data['reservation_code']}`
                console.log(`[ ${data.transaction_id} ] failed process onboard, un eligible reservation-code, task with topic: onboard-plane`)
                await producer.send({
                    topic: 'boarding-transaction.reply',
                    messages: [
                        {
                            key: data.id,
                            value: JSON.stringify(data),
                            partition: 0
                        },
                    ],
                })
                await taskService.complete(task)
            }
        } catch (error) {
            console.log([error], `error process onboard workflow, task with topic: onboard-plane, ${data.transaction_id}`)
            data.message = TransactioEnum.MessageBookingTransaction.FAILED_REDEEM_TRANSACTION
            data['detail_message'] = `error process onboard workflow, task with topic: onboard-plane, ${data.transaction_id}`
            await producer.send({
                topic: 'boarding-transaction.reply',
                messages: [
                    {
                        key: data.id,
                        value: JSON.stringify(data),
                        partition: 0
                    },
                ],
            })
            await taskService.complete(task)

        }
    })

} catch (error) {
    console.log(error)
}


