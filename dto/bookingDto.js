class BookingDto {
  constructor(Reservation, Passenger) {
    this.reservation = Reservation;
    this.passenger = Passenger;
  }
}

export default BookingDto;