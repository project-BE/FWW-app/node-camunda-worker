class Passenger {
  constructor(first_name, last_name, email, phone_number, nik) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.email = email;
    this.phone_number = phone_number;
    this.nik = nik;
  }
}

export default Passenger