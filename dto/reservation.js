class Reservation {
    constructor(flight_id, transaction_id, booking_code, seat_id, user_id) {
      this.flight_id = flight_id;
      this.transaction_id = transaction_id;
      this.booking_code = booking_code;
      this.seat_id = seat_id;
      this.user_id = user_id;
    }
  }
  
  export default Reservation;