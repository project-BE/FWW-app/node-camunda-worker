export const paymentMethod = [
    {
        payment_method_id: 'AAA111',
        payment_method_name: 'credit card payment',
        payment_method_info: 'some information',

    },
    {
        payment_method_id: 'BBB222',
        payment_method_name: 'virtual account',
        payment_method_info: 'some information',

    },
    {
        payment_method_id: 'CCC333',
        payment_method_name: 'electronic wallet',
        payment_method_info: 'some information',

    },

]